<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="index" class="sidebar-brand">
                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>IO-</strong>T</span>
            </a>
            <!-- END Brand -->

            <!-- Theme Colors -->
            <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
            <ul class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide">
                <!-- You can also add the default color theme
                <li class="active">
                    <a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                </li>
                -->
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-night themed-border-night" data-theme="assets/css/themes/night.css" data-toggle="tooltip" title="Night"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-amethyst themed-border-amethyst" data-theme="assets/css/themes/amethyst.css" data-toggle="tooltip" title="Amethyst"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-modern themed-border-modern" data-theme="assets/css/themes/modern.css" data-toggle="tooltip" title="Modern"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-autumn themed-border-autumn" data-theme="assets/css/themes/autumn.css" data-toggle="tooltip" title="Autumn"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-flatie themed-border-flatie" data-theme="assets/css/themes/flatie.css" data-toggle="tooltip" title="Flatie"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-spring themed-border-spring" data-theme="assets/css/themes/spring.css" data-toggle="tooltip" title="Spring"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-fancy themed-border-fancy" data-theme="assets/css/themes/fancy.css" data-toggle="tooltip" title="Fancy"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-fire themed-border-fire" data-theme="assets/css/themes/fire.css" data-toggle="tooltip" title="Fire"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-coral themed-border-coral" data-theme="assets/css/themes/coral.css" data-toggle="tooltip" title="Coral"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-lake themed-border-lake" data-theme="assets/css/themes/lake.css" data-toggle="tooltip" title="Lake"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-forest themed-border-forest" data-theme="assets/css/themes/forest.css" data-toggle="tooltip" title="Forest"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-waterlily themed-border-waterlily" data-theme="assets/css/themes/waterlily.css" data-toggle="tooltip" title="Waterlily"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-emerald themed-border-emerald" data-theme="assets/css/themes/emerald.css" data-toggle="tooltip" title="Emerald"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="themed-background-dark-blackberry themed-border-blackberry" data-theme="assets/css/themes/blackberry.css" data-toggle="tooltip" title="Blackberry"></a>
                </li>
            </ul>
            <!-- END Theme Colors -->

            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li>
                    <a href="#"><i class="gi gi-dashboard sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Menu 1</span></a>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="hi hi-list-alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Managers</span></a>
                    <ul>
                        <li>
                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left  sidebar-nav-indicator"></i></i> User Managers</a>
                            <ul>
                                <li>
                                    <a href="user-managers">User</a>
                                </li>
                                <li>
                                    <a href="user-upload">Bulk Upload</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Department Manager</a>
                        </li>
                        <li>
                            <a href="#">Property Manager</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-settings sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Settings</span></a>
                    <ul>
                        <li>
                            <a href="settings-governance">Governance</a>
                        </li>
                        <li>
                            <a href="#">Application</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- END Sidebar Navigation -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>